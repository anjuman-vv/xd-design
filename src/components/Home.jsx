import React from 'react'
import './home.css'
import './Form.css'
import './footer.css'
import logo from "../assest/navimg/Group 174.png"
import searchlogo from '../assest/search/search.png'
import searchlogo2 from '../assest/search/search.svg'
import course1 from './img/ec608cb3bf2b553ebf2517855b629a46.png'
import course2 from './img/52f8897dd449275d90586175ec3e63a5.png'
import course3 from './img/1a29ddff6716c403374c5958de8af701.png'
import course4 from './img/0855f10c1aea0387ab9091bb3f5b3f1f.png'
import course5 from './img/cb9403bd53a89cc479036fb475acba7d.png'
import course6 from './img/daf9c0858845c2fac42d80dc4d694736.png'
import course7 from './img/0855f10c1aea0387ab9091bb3f5b3f1f.png'
import course8 from './img/cb9403bd53a89cc479036fb475acba7d.png'
import course9 from './img/daf9c0858845c2fac42d80dc4d694736.png'
import rightpath from '../assest/HomeScreen/Path@2x.png'
import left_path from '../assest/Path/Path@2x.png'
import line52 from '../assest/img/Line 52.svg'
import Union5 from '../assest/img/Union 5.svg'
import hero from '../assest/herobackground/ad910b4f657bb57be5f76bd4dc44c288.png'

export const Home = () => {
  return (
    <>
   <nav className='navbar'>
      <ul>
         <li class="navlogo"><img class="logo_image" src={logo} alt="logo" /></li>
         <li class="searchitems">
            <img src={searchlogo} alt="" />
            <input type="text" placeholder='Search' /></li>
         <li class="courses">Courses</li>
         <li class="techoninskillz">Tech on Inskillz</li>
         <li class="contact">Contact</li>
         <li class="navbtn"><button>Login/signup</button></li>
      </ul>
   </nav>

   {/* hero */}

   <section className='herosections'>
    <div className='background'>
      <img src={hero} alt="" />
      
       </div>
       <div className='content'>
        <p className='headingtext'>courses offered</p>
        <p className='para'>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry</p>

        </div>
   </section>


   <section className='view_Our_courses'>
     <p>View Our Courses</p> 
     <div>
      <div type="text" className='search'>
      <img src={searchlogo2} alt="" className='searchlogo2'/>
      <input type="text" name="" id="" placeholder='Search'/>
      <button>By Name</button>
      </div> 
      <div className='littlecircle'></div>
      <p className='littlecircleText'>Show only New courses</p>
      </div>
      <div className='courses_text_heading_cot'>
         <h3>Categories</h3>
         <p>Photography</p>
         <p></p>
         <p>Digital Photography(11)</p>
      </div>
      <hr />
      <div className='courses_containers'>
      <div  style={{ width:"10%"}}>
         <div className='right_path1'>
            <img src={rightpath} alt="" />
         </div>
      </div>
      <div className='sidebar' style={{width:"15%" ,height:'auto', marginTop:'1rem'}}>
         <div style={{
           display: 'flex',
           justifyContent: 'space-between',
            
         }}>
            <p style={{color : "#143754"}}>All</p>
            <p style={{color : "#143754"}}>1234</p>
         </div>

         <div className='photography_blue_box'>
            <div style={{
           display: 'flex', 
           
         }}>
            <img src={line52} alt="" />
            <p className='phtog'>Photoghaphy</p>
            <p className='n23'>23</p>
            </div>
            <div style={{
           display: 'flex',
           marginTop :'3vh'
           }}>
            <p className='di'>DigitalPhotoghaphy</p>
            <p className='n11'>11</p>
            </div>
            <div style={{
           display: 'flex',
           marginTop :'4vh'
           }}>
               <p className='wildlife'>wildlife Photoghaphy</p>
               <p className='n12'>12</p>
            </div>
         </div>

         <div className='div_one'>
            <p>+</p>
            <p>Illustration</p>
            <p>33</p>
         </div>
         <div className='div_two'>
            <p>Graphic Design</p>
            <p>12</p>
         </div>
         <div className='div_two'>
         <p>Graphic Design</p>
            <p>12</p>
         </div>
         <div className='div_two'>
         <p>Graphic Design</p>
            <p>12</p>
         </div>
         <div className='div_two'>
         <p>Graphic Design</p>
            <p>12</p>
         </div>
         <div className='div_two'>
         <p>Graphic Design</p>
            <p>12</p>
         </div>
         <div className='div_two'>
         <p>Graphic Design</p>
            <p>12</p>
         </div>
         <div className='div_two'>
         <p>Graphic Design</p>
            <p>12</p>
         </div>
         <div className='div_two'>
         <p>Graphic Design</p>
            <p>12</p>
         </div>
         <div className='div_two'>
         <p>Graphic Design</p>
            <p>12</p>
         </div>
      </div>


      <div className='mid_div_courses' style={{width:"75%", }}>
        
         <div>
         <div className='course1'>
            <img src={course1} alt="" />
            <div className='course1Detail'>
               <h3>Why Digital Marketing ?</h3>
               <p className='byinstitutename'>By Institute Name</p>
               <div 
               style={{
                  display: 'flex',
                   marginLeft:'1vh',
                   marginTop:'1vh',
                   
                   }}>
               <div className='star'></div>
               <div className='star'></div>
               <div className='star'></div>
               <div className='star'></div>
               <div className='star'></div>
               <p className='rating'>5.0</p>
               <p className='view_coursesp'>View Course</p>
               </div>  
            </div>

         
         </div>

         
         <div className='course2'>
         <img src={course2} alt="" />
            <div className='course1Detail'>
               <h3>Why Digital Marketing ?</h3>
               <p className='byinstitutename'>By Institute Name</p>
               <div style={{
                  display: 'flex',
                   marginLeft:'1vh',
                   marginTop:'1vh',
                   
                   }}>
               <div className='star'></div>
               <div className='star'></div>
               <div className='star'></div>
               <div className='star'></div>
               <div className='star'></div>
               <p className='rating'>5.0</p>
               <p className='view_coursesp'>View Course</p>
               </div>  
            </div>
         </div>
         <div className='course3'>
         <img src={course3} alt="" />
            <div className='course1Detail'>
               <h3>Why Digital Marketing ?</h3>
               <p className='byinstitutename'>By Institute Name</p>
               <div style={{
                  display: 'flex',
                   marginLeft:'1vh',
                   marginTop:'1vh',
                   
                   }}>
               <div className='star'></div>
               <div className='star'></div>
               <div className='star'></div>
               <div className='star'></div>
               <div className='star'></div>
               <p className='rating'>5.0</p>
               <p className='view_coursesp'>View Course</p>
               </div>  
            </div>
         </div>
         </div>
         

      
         <div>
         <div className='course4'>
            <img src={course4} alt="" />
            <div className='course1Detail'>
               <h3>Why Digital Marketing ?</h3>
               <p className='byinstitutename'>By Institute Name</p>
               <div style={{
                  display: 'flex',
                   marginLeft:'1vh',
                   marginTop:'1vh',
                   
                   }}>
               <div className='star'></div>
               <div className='star'></div>
               <div className='star'></div>
               <div className='star'></div>
               <div className='star'></div>
               <p className='rating'>5.0</p>
               <p className='view_coursesp'>View Course</p>
               </div>  
            </div>

         
         </div>

         
         <div className='course5'>
         <img src={course5} alt="" />
            <div className='course1Detail'>
               <h3>Why Digital Marketing ?</h3>
               <p className='byinstitutename'>By Institute Name</p>
               <div style={{
                  display: 'flex',
                   marginLeft:'1vh',
                   marginTop:'1vh',
                   
                   }}>
               <div className='star'></div>
               <div className='star'></div>
               <div className='star'></div>
               <div className='star'></div>
               <div className='star'></div>
               <p className='rating'>5.0</p>
               <p className='view_coursesp'>View Course</p>
               </div>  
            </div>
         </div>
         <div className='course6'>
         <img src={course6} alt="" />
            <div className='course1Detail'>
               <h3>Why Digital Marketing ?</h3>
               <p className='byinstitutename'>By Institute Name</p>
               <div style={{
                  display: 'flex',
                   marginLeft:'1vh',
                   marginTop:'1vh',
                   
                   }}>
               <div className='star'></div>
               <div className='star'></div>
               <div className='star'></div>
               <div className='star'></div>
               <div className='star'></div>
               <p className='rating'>5.0</p>
               <p className='view_coursesp'>View Course</p>
               </div>  
            </div>
         </div>
         </div>   
      
         <div>
         <div className='course7'>
            <img src={course7} alt="" />
            <div className='course1Detail'>
               <h3>Why Digital Marketing ?</h3>
               <p className='byinstitutename'>By Institute Name</p>
               <div style={{
                  display: 'flex',
                   marginLeft:'1vh',
                   marginTop:'1vh',
                   
                   }}>
               <div className='star'></div>
               <div className='star'></div>
               <div className='star'></div>
               <div className='star'></div>
               <div className='star'></div>
               <p className='rating'>5.0</p>
               <p className='view_coursesp'>View Course</p>
               </div>  
            </div>

         
         </div>

         
         <div className='course8'>
         <img src={course8} alt="" />
            <div className='course1Detail'>
               <h3>Why Digital Marketing ?</h3>
               <p className='byinstitutename'>By Institute Name</p>
               <div style={{
                  display: 'flex',
                   marginLeft:'1vh',
                   marginTop:'1vh',
                   
                   }}>
               <div className='star'></div>
               <div className='star'></div>
               <div className='star'></div>
               <div className='star'></div>
               <div className='star'></div>
               <p className='rating'>5.0</p>
               <p className='view_coursesp'>View Course</p>
               </div>  
            </div>
         </div>
         <div className='course9'>
         <img src={course9} alt="" />
            <div className='course1Detail'>
               <h3>Why Digital Marketing ?</h3>
               <p className='byinstitutename'>By Institute Name</p>
               <div style={{
                  display: 'flex',
                   marginLeft:'1vh',
                   marginTop:'1vh',
                   
                   }}>
               <div className='star'></div>
               <div className='star'></div>
               <div className='star'></div>
               <div className='star'></div>
               <div className='star'></div>
               <p className='rating'>5.0</p>
               <p className='view_coursesp'>View Course</p>
               </div>  
            </div>
         </div>
         </div>


      
      </div>



      <div className='left_path2' style={{width:"10%", }}>
         <img src={left_path} alt="" />
      </div>
      </div>
      
     
       
         
   </section>


   <section>
   <div className='form_section'>
        <div className='form_path_corner'>
         <img src={rightpath} alt="" />
         
        </div>
        <div className='form_background_img'>
            <div className='form_container'>
            <div className='disscusHeading'><p>Let's Discuss</p></div>
            <div className='disscuspara'><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p></div>
            <div className='name_email_cont' style={{ marginTop:"1rem"}}>
               <input type="text" name="" className='Name' placeholder='Fazil V.N'/>
               <input type="text" name="" className="Email" placeholder='Email' />
            </div>

            <div style={{  marginTop:"2rem"}}>
            <input type="text" name="" className='Phone Number' placeholder='name'/>
               <input type="select" name="" className="EnquryType" placeholder=' Select Enquiry Type' />
            </div>

            <div className='textarearcont'>
                <input type="text" className='textarea' placeholder='Message'/>
            </div>

            <div className='form_submit_btn'>
              <button>Submit</button></div>
              
            </div>
        </div>
    </div>
   </section>





   <footer>
    <div className='footer'>

      <div className='columdiv1'>
      <p className='footerhead1'>Download The APP Now</p>
      <div className='apple'></div>
      <div className='android'></div>
      <p className='copyright'>© Copyright SketchMonk 2021 Design</p>
      </div>

      <div className='columdiv2'>
        <p className='general'>General</p>
        <p className='signup'>Signup</p>
        <p className='aboutus'>About Us</p>
        <p className='tectoninskillz'>Teach On Inskillz</p>
        <p className='contactp'>Contact Us</p>
        <p className='careers'>Careers</p>
        
      </div>
      <div className='columdiv3'>
      <p className='community'>Community</p>
      <p className='createAcccount'>Create Account</p>
      <p className='joinpremium'>Join Premium</p>
      <p className='referstofriend'>Refer A Friend</p>
      </div>
      <div className='columdiv4'>
        <p className='support'>Support</p>
        <p className='fqs'>FAQ's</p>
        <p className='termsconditions'>Terms & Condition</p>
        <p className='privacypolicy'>Privacy Policy</p>
        <p className='gethelp'>Get Hepl</p>
      </div>
      <div className='columdiv5'>
        <p className='contactus'>Contact Us</p>
        <p className='address'>FutureLabs Interactive Pvt. Ltd No. 6/858-M, 2nd Floor, Valamkottil Towers, Judgemukku, Kakkanad, Kochi</p>
        <iframe src="https://maps.google.com/maps?q=ahemdabad&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" className='map'></iframe>
        <div className='socialmedia'>
        <ul className='facebook'></ul>
          <ul className='instagram'></ul>
          <ul className='linkdin'></ul>
          <ul className='youtube'></ul>
        </div>
      </div>
        
       
    </div>
   </footer>


   </>  
  )
}


      // <div className='cat_photo_digital_text_div_container'>
      //       <p>Categories</p>
      //       <p>Photography</p>
      //       <p>Digital Photography(11)</p>
      //    </div>